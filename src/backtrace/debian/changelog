rust-backtrace (0.3.68-2) unstable; urgency=medium

  * Team upload.
  * Package backtrace 0.3.68 from crates.io using debcargo 2.6.0
  * Bump flate2 dependency to help britney's autopkgtest scheduler.

 -- Peter Michael Green <plugwash@debian.org>  Wed, 30 Aug 2023 23:46:15 +0000

rust-backtrace (0.3.68-1) unstable; urgency=medium

  * Package backtrace 0.3.68 from crates.io using debcargo 2.6.0

  [ Fabian Grünbichler ]
  * Team upload.
  * Package backtrace 0.3.68 from crates.io using debcargo 2.6.0

 -- Peter Michael Green <plugwash@debian.org>  Mon, 28 Aug 2023 12:35:20 +0000

rust-backtrace (0.3.67-2) unstable; urgency=medium

  * Team upload.
  * Package backtrace 0.3.67 from crates.io using debcargo 2.6.0
  * Add depends on librust-flate2-dev (>= 1.0.25) to help britney's autopkgtest
    scheduler.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 03 Jan 2023 10:03:47 +0000

rust-backtrace (0.3.67-1) unstable; urgency=medium

  * Team upload.
  * Package backtrace 0.3.67 from crates.io using debcargo 2.6.0
  * Drop relax-dep.diff, no longer needed.

 -- Peter Michael Green <plugwash@debian.org>  Mon, 02 Jan 2023 17:02:09 +0000

rust-backtrace (0.3.66-2) unstable; urgency=medium

  * Team upload.
  * Package backtrace 0.3.66 from crates.io using debcargo 2.5.0
  * Stop reducing miniz-oxide dependency.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 18 Oct 2022 15:48:55 +0000

rust-backtrace (0.3.66-1) unstable; urgency=medium

  * Team upload.
  * Package backtrace 0.3.66 from crates.io using debcargo 2.5.0
  * Drop adjust-features.diff.
  * Remove broken marker from gimli-symbolize feature (Closes: 1010099)
  * Bump addr2line dependency to 0.18
  * Reduce miniz-oxide dependency to 0.4
 
 -- Peter Michael Green <plugwash@debian.org>  Sun, 21 Aug 2022 03:38:14 +0000

rust-backtrace (0.3.51-1) experimental; urgency=medium

  * Team upload.
  * Package backtrace 0.3.51 from crates.io using debcargo 2.4.4
  * Adjust features/dependencies to avoid creating any new binary
    packages.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 02 Nov 2021 04:49:12 +0000

rust-backtrace (0.3.44-6) unstable; urgency=medium

  * Team upload.
  * Package backtrace 0.3.44 from crates.io using debcargo 2.4.2
  * Fix feature name error in debcargo.toml, the rust feature
    is named compiler_builtins not compiler-builtins.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 14 Apr 2020 20:56:43 +0000

rust-backtrace (0.3.44-5) unstable; urgency=medium

  * Team upload.
  * Package backtrace 0.3.44 from crates.io using debcargo 2.4.2
  * Mark test as broken for compiler-builtins featureset, I missed it
    in the last upload.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 14 Apr 2020 19:43:13 +0000

rust-backtrace (0.3.44-4) unstable; urgency=medium

  * Team upload.
  * Package backtrace 0.3.44 from crates.io using debcargo 2.4.2
  * Mark tests as broken for some featuresets.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 14 Apr 2020 10:24:49 +0000

rust-backtrace (0.3.44-3) unstable; urgency=medium

  * Team upload.
  * Package backtrace 0.3.44 from crates.io using debcargo 2.4.2
  * Stick with 0.3.44 in unstable until a sane 0.3.46 package passes NEW
    in experimental.
  * Remove "rustc-dep-of-std" featureset packages
  * Relax dependency on goblin

 -- Peter Michael Green <plugwash@debian.org>  Mon, 13 Apr 2020 02:31:04 +0000

rust-backtrace (0.3.44-2) unstable; urgency=medium

  * Package backtrace 0.3.44 from crates.io using debcargo 2.4.2
  * Add a relax-dep patch for dependencies that are in Debian in older
    versions

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Thu, 20 Feb 2020 10:01:20 +0100

rust-backtrace (0.3.44-1) unstable; urgency=medium

  * Package backtrace 0.3.44 from crates.io using debcargo 2.4.2
  * Drop obsolete dependency upgrade patch

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Wed, 19 Feb 2020 10:58:52 +0100

rust-backtrace (0.3.40-2) unstable; urgency=medium

  * Team upload.
  * Package backtrace 0.3.40 from crates.io using debcargo 2.4.2
  * Depend on goblin 0.1 as that's the version we have in Debian.

 -- Ximin Luo <infinity0@debian.org>  Sat, 04 Jan 2020 22:10:40 +0000

rust-backtrace (0.3.40-1) unstable; urgency=medium

  * Package backtrace 0.3.40 from crates.io using debcargo 2.4.0
  * Closes: #935672

 -- Wolfgang Silbermayr <wolfgang@silbermayr.at>  Tue, 24 Dec 2019 21:20:07 +0000

rust-backtrace (0.3.34-1) unstable; urgency=medium

  * Team upload.
  * Package backtrace 0.3.34 from crates.io using debcargo 2.3.1-alpha.0

  [ kpcyrd ]
  * Package backtrace 0.3.33 from crates.io using debcargo 2.4.0

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 11 Aug 2019 11:41:02 +0200

rust-backtrace (0.3.26-1) unstable; urgency=medium

  * Team upload.
  * Package backtrace 0.3.26 from crates.io using debcargo 2.2.10

 -- Ximin Luo <infinity0@debian.org>  Thu, 30 May 2019 22:53:46 -0700

rust-backtrace (0.3.13-1) unstable; urgency=medium

  * Team upload.
  * Package backtrace 0.3.13 from crates.io using debcargo 2.2.9

 -- Ximin Luo <infinity0@debian.org>  Wed, 26 Dec 2018 16:35:03 -0800

rust-backtrace (0.3.9-1) unstable; urgency=medium

  * Package backtrace 0.3.9 from crates.io using debcargo 2.2.7

 -- kpcyrd <git@rxv.cc>  Thu, 20 Sep 2018 11:24:20 +0200
